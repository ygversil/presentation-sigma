:title: Développement en Python d'un catalogue web de données géolocalisées
:author: Yann Voté
:data-transition-duration: 1500
:css: css/presentation_sigma.css

----

:id: title

.. image:: img/logo_ensat.png
   :class: inline-logo

.. image:: img/logo_univ_tlse.png
   :class: inline-logo

.. image:: img/logo_ut2j.png
   :class: inline-logo

Développement en Python d'un catalogue web de données géolocalisées
====================================================================

.. image:: img/logo_logilab.png

Yann Voté
---------

Master 2 SIGMA - Septembre 2015
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

----

Sommaire
========

#. Introduction

   * Logilab

   * Objectifs

#. Modèle de données

#. Principes du moissonnage

#. Affichage des géométries & filtre géographique

#. Perspectives et conclusion

----

Logilab et CubicWeb
===================

.. image:: img/logilab.png
   :width: 100%
   :align: center

CubicWeb
--------

Un *framework* Python de développement web facilitant les réutilisations.

----

Objectifs
=========

**Contexte**
    montée de l'*open data*, multiplication des portails


* Aggréger les données en provenance de plusieurs catalogues *open data*.

* Permettre l'affichage et la recherche de données géolocalisées dans
  CubicWeb.

----

Pourquoi aggréger ?
===================

* permet la recherche dans de multiples bases

* offre un point d'accès unique

* facilite les liens, le croisement d'informations

----

:data-x: -1000
:data-rotate: 90
:data-scale: .6

:class: center

Démonstration vidéo
===================

.. image:: img/play.png
   :width: 150px
   :align: center
   :target: img/demo_sigma.mp4

----

:data-z: r1000
:data-y: r1000
:data-rotate-y: 90
:data-scale: 1

:class: center

Différents modèles de données
=============================

.. image:: img/udata_model.png
   :height: 200px
   :align: center

data.gouv.fr


.. image:: img/rdf_dcat_model.png
   :height: 200px
   :align: center

open-data.europa.eu

----

:data-z: r1000
:data-x: r0
:data-y: r0

:class: center

DCAT
====

Une recommandation du W3C
-------------------------

.. image:: img/dcat_model.jpg
   :width: 75%
   :align: center

----

:class: center

Le modèle de données utilisé
============================

.. image:: img/cubicweb_datacat_model.png
   :width: 100%
   :align: center

----

:data-x: r-4000
:data-y: r-4000
:data-z: r-4000
:data-rotate-y: r90
:data-scale: 0.4

:class: center

Moissonnage
===========


    *Télécharger, adapter et insérer dans ses propres bases des données
    provenant d'une source externe*


.. image:: img/extract.png
   :class: inlined


.. image:: img/transform.png
   :class: inlined


.. image:: img/load.png
   :class: inlined

=============== == ================ == ================
Données sources -> Entités Externes -> Entités CubicWeb
=============== == ================ == ================

----

:data-x: r-400
:data-y: r400
:data-z: r400
:data-rotate-y: r0


.. image:: img/extract.png
   :width: 150px
   :class: right

Téléchargement
==============

**Principe**
    générer un flux d'entités externes grâce à l'instruction ``yield``

.. code:: python
   :class: twocols

   # Don't do that!
   def extract(url):
       fetch_dsets = fetch(url)
       exts = []
       for dset in fetch_dsets:
           ext = process(dset)
           exts.append(ext)
       return exts

.. code:: python
   :class: twocols

   # Much better!
   def extract(url):
       fetch_dsets = fetch(url)
       for dset in fetch_dsets:
           ext_entity = process(dset)
           yield ext_entity

----

.. image:: img/transform.png
   :width: 150px
   :class: right

Nettoyage
=========

**Principe**
    brancher dans le flux d'entités des fonctions de signature ``ExtEntity -> ExtEntity``

.. code:: python
   :class: twocols

   def latlong_to_wkt(ext_entity):
       new = ext_entity.copy()
       # Convert lat/long -> wkt
       # ...
       return new

.. code:: python
   :class: twocols

   def transform(ext_entities):
       for ext_entity in ext_entities
           tmp = latlong_to_wkt(ext_entity)
           tmp = other_function1(tmp)
           tmp = other_function2(tmp)
           # Other plugged functions
           # ...
           yield tmp

----

.. image:: img/load.png
   :width: 150px
   :class: right

Insertion
=========

**Principe**
    Utiliser la méthode ``import_entities()`` fournie par CubicWeb

.. code:: python

   def load(ext_entities):
       importer = ExtEntitiesImporter()
       importer.import_entities(ext_entities)

----

Moissonnage complet
===================

.. code:: python

   def harvest(url):
       raw_ext_entities = extract(url)
       ext_entities = transform(raw_ext_entities)
       load(ext_entities)

----

:data-x: r-800
:data-y: r-800
:data-z: r-2000
:data-rotate-x: r90
:data-scale: 0.2

Affichage des géométries
========================

.. image:: img/leaflet.png
   :width: 150px
   :class: right

**Principe**
    convertir les entités en GeoJSON + utiliser le cube Leaflet (webmapping)

============== == =====================
Localisation   -> *Feature*
Jeu de données -> *FeatureCollection*
============== == =====================

**Exemple**
    ::

        Location(name='France', geometry='Point (2 46)')

    devient::

        {type: "Feature", properties: {name: "France"},
         geometry: {type: "Point", coordinates: [2, 46]}}

----

:data-x: r0
:data-y: r0
:data-z: r-400
:data-rotate-x: r0

Filtre géographique
===================

.. image:: img/facet.png
   :width: 150px
   :class: right

**Principe**
    extension ``Leaflet.Draw`` + ajout de la restriction ``ST_INTERSECTS()``

**Déroulement**

    #. L'utilisateur affiche une liste de jeux de données. Cette liste résulte
       d'une requête SQL::

           SELECT dataset.title FROM dataset WHERE dataset.theme = 'Transport'

    #. L'utilisateur dessine un polygone dans la carte (``POLYGON (...)``). Cela
       ajoute une restriction::

           SELECT dataset.title FROM dataset WHERE dataset.theme = 'Transport'
           AND ST_INTERSECTS(dataset.geometry, ST_GEOMFROMTEXT('POLYGON(...)'))

----

:data-x: r-3000
:data-y: r-3000
:data-z: r-3000
:data-rotate-y: r90
:data-scale: 2

:class: center

Perspectives
============

.. image:: img/solution.png
   :height: 500px
   :align: center

----

:data-x: r0
:data-y: r-3000
:data-z: r0
:data-rotate-y: r0

Conclusion
==========

* Projet riche: python, Postgis, Leaflet, algorithmique, modélisation des
  données (E/R), modélisation UML

* beaucoup de difficultés rencontrées => le chemin est encore long

----

:class: center

Fin
===


    *Merci de votre attention*
